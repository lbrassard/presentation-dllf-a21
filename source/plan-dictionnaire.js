const PLAN_DICTIONNAIRE = {
  'C8160-1': {
    'etiquette': 'CRILCQ',
    'departement': 'CRILCQ'
  },
  'C8160': {
    'etiquette': 'CRILCQ',
    'departement': 'CRILCQ'
  },
  'C8150': {
    'etiquette': 'CRILCQ',
    'departement': 'CRILCQ'
  },
  'C8141-C8151': {
    'etiquette': 'CRILCQ',
    'extra': '<img src="img/periodiques-et-poste-de-travail.jpg" /> <img src="img/collection-de-zines.jpg" />',
    'departement': 'CRILCQ'
  },
  'C8132': {
    'etiquette': '',
    'departement': 'DLLM',
  },
  'C8130': {
    'etiquette': 'R. Swartzwaldz',
    'departement': 'DLLM'
  },
  'C8128': {
    'etiquette': 'Lianne Moyes',
    'departement': 'DLLM'
  },
  'C8126': {
    'etiquette': 'Irving Wolfe',
    'departement': 'DLLM'
  },
  'C8124': {
    'etiquette': 'Joyce Boro',
    'departement': 'DLLM'
  },
  'C8122': {
    'etiquette': 'Heike Harting',
    'departement': 'DLLM'
  },
  'C8120': {
    'etiquette': 'Jane Malcolm',
    'departement': 'DLLM'
  },
  'C8118': {
    'etiquette': 'Heather Meek',
    'departement': 'DLLM'
  },
  'C8116': {
    'etiquette': 'Sarah Henzi',
    'departement': 'DLLM'
  },
  'C8114': {
    'etiquette': 'Gail Scott',
    'departement': 'DLLM'
  },
  'C8112': {
    'etiquette': 'Najat Rahman',
    'departement': 'DLLM'
  },
  'C8110': {
    'etiquette': 'J-C Guédon',
    'departement': 'DLLM'
  },
  'C8108': {
    'etiquette': 'B. Agnese',
    'departement': 'DLLM'
  },
  'C8106': {
    'etiquette': 'Maria Zinfert',
    'departement': 'DLLM'
  },
  'C8104': {
    'etiquette': 'Éric Savoy',
    'departement': 'DLLM'
  },
  'C8102': {
    'etiquette': 'J. Cardinal',
    'departement': 'DLLM'
  },
  'C8100': {
    'etiquette': 'Bernard Dupriez',
    'departement': 'DLLM'
  },
  'C8098': {
    'etiquette': 'Tonglin Lu',
    'departement': 'DLLM'
  },
  'C8096': {
    'etiquette': 'TA',
    'departement': 'DLLM'
  },
  'C8094': {
    'etiquette': 'TGDE',
    'departement': 'DLLM'
  },
  'C8092': {
    'etiquette': 'TGDE',
    'departement': 'DLLM'
  },
  'C8088': {
    'etiquette': 'Directeur/rice DLMO',
    'departement': 'DLLM'
  },
  'C8088-1': {
    'etiquette': 'Salle de réunion',
    'departement': 'DLLM'
  },
  'C8086': {
    'etiquette': 'Secrétariat DLMO',
    'departement': 'DLLM'
  },
  'C8086-1': {
    'etiquette': 'Karine Jetté',
    'departement': 'DLLM'
  },
  'C8082': {
    'etiquette': 'Salle de réunion',
    'departement': 'DLLM'
  },
  'C8069': {
    'etiquette': 'T. Teixera',
    'departement': 'DLLM'
  },
  'C8071': {
    'etiquette': 'J-Carlos Godenzzi',
    'departement': 'DLLM'
  },
  'C8073': {
    'etiquette': 'A. B. M. Sevillano',
    'departement': 'DLLM'
  },
  'C8075': {
    'etiquette': 'H. Palto Maldonado',
    'departement': 'DLLM'
  },
  'C8077': {
    'etiquette': 'James Cisneros',
    'departement': 'DLLM'
  },
  'C8079': {
    'etiquette': 'A. Alba de la Fuente',
    'departement': 'DLLM'
  },
  'C8076-C8078': {
    'etiquette': 'Salle de réunion Carderrera',
    'departement': 'DLLM'
  },
  'C8074': {
    'etiquette': 'E. V-Castellanas',
    'departement': 'DLLM'
  },
  'C8072': {
    'etiquette': 'Javier Rubiera',
    'departement': 'DLLM'
  },
  'C8070': {
    'etiquette': '',
    'departement': 'DLLM'
  },
  'C8068': {
    'etiquette': 'Cuisine 1',
    'departement': 'DLLM'
  },
  'C8068-1': {
    'etiquette': 'Cuisine 2',
    'departement': 'DLLM'
  },
  'C8056': {
    'etiquette': 'Centre culturel',
    'departement': 'DLLM'
  },
  'C8052': {
    'etiquette': 'Jürgen Heizmann',
    'departement': 'DLLM'
  },
  'C8050': {
    'etiquette': 'Manuel Meune',
    'departement': 'DLLM'
  },
  'C8134': {
    'etiquette': '',
    'departement': 'DLLM'
  },
  'C8057': {
    'etiquette': 'G. Lodi',
    'departement': 'DLLM'
  },
  'C8059': {
    'etiquette': 'J.Bouchard, T.Van Rahden',
    'departement': 'DLLM'
  },
  'C8081': {
    'etiquette': 'CRI Intermédialités',
    'departement': 'DLLM'
  },
  'C8083': {
    'etiquette': 'Nikola Von Mervelt',
    'departement': 'DLLM'
  },
  'C8085': {
    'etiquette': 'Photocopies',
    'departement': 'DLLM'
  },
  'C8095': {
    'etiquette': 'Telecom',
    'departement': 'DLLM'
  },
  'C8101-C8103': {
    'etiquette': 'Centre de ressources de l’espagnol',
    'departement': 'DLLM'
  },
  'C8105': {
    'etiquette': 'Philippe Despoix / Intermédialités',
    'departement': 'DLLM'
  },
  'C8107': {
    'etiquette': 'R-Livia Monnet',
    'departement': 'DLLM'
  },
  'C8109-C8111': {
    'etiquette': 'Salle de réunion',
    'departement': 'DLLM'
  },
  'C8113': {
    'etiquette': 'Terry Cochran',
    'departement': 'DLLM'
  },
  'C8115': {
    'etiquette': 'Simon Harel',
    'departement': 'DLLM'
  },
  'C8117': {
    'etiquette': 'C. Brown',
    'departement': 'DLLM'
  },
  'C8119': {
    'etiquette': 'Chargés de cours DLMO',
    'departement': 'DLLM'
  },
  'C8121': {
    'etiquette': 'Michael Sinatra',
    'departement': 'DLLM'
  },
  'C8123': {
    'etiquette': ' Salle de réunion',
    'departement': 'DLLM'
  },
  'C8125': {
    'etiquette': 'Étudiants au doctorat',
    'departement': 'DLLM'
  },
  'C8133': {
    'etiquette': 'Réserve',
    'departement': 'DLLM'
  },
  'C8137': {
    'etiquette': '',
    'departement': 'DLLM'
  },
  'C8047': {
    'etiquette': 'Archives',
    'departement': 'DLLF_Admin'
  },
  'C8055': {
    'etiquette': 'Remise',
    'departement': 'DLLF_Admin'
  },
  'C8016': {
    'etiquette': 'TGDE 1er cycle',
    'departement': 'DLLF_Admin'
  },
  'C8014': {
    'etiquette': 'TGDE cycles supérieurs',
    'departement': 'DLLF_Admin'
  },
  'C8010': {
    'etiquette': 'Directeur/trice',
    'departement': 'DLLF_Admin'
  },
  'C8011': {
    'etiquette': 'Photocopie',
    'departement': 'DLLF_Admin'
  },
  'C8001': {
    'etiquette': 'Adjoint·e au directeur/trice',
    'departement': 'DLLF_Admin'
  },
  'C8008': {
    'etiquette': 'Secrétariat',
    'extra': '<img src="img/photos/secretariat2.jpg" /> <img src="img/photos/secretariat1.jpg" />',
    'departement': 'DLLF_Admin'
  },
  'C8008-1': {
    'etiquette': 'TCTB DLLF',
    'departement': 'DLLF_Admin'
  },
  'C8008-2': {
    'etiquette': 'TA DLLF',
    'departement': 'DLLF_Admin'
  },
  'C8008-3': {
    'etiquette': 'Voûte',
    'departement': 'DLLF_Admin'
  },
  'C8152': {
    'etiquette': 'G. Giannini',
    'departement': 'DLLF'
  },
  'C8148': {
    'etiquette': 'A. Viaud',
    'departement': 'DLLF'
  },
  'C8146': {
    'etiquette': 'C. Legendre',
    'departement': 'DLLF'
  },
  'C8144': {
    'etiquette': 'G. Dupuis',
    'departement': 'DLLF'
  },
  'C8140': {
    'etiquette': 'M-E. Lapointe',
    'departement': 'DLLF'
  },
  'C8045': {
    'etiquette': 'F. Gingras',
    'departement': 'DLLF'
  },
  'C8048': {
    'etiquette': 'Lucie Bourassa',
    'departement': 'DLLF'
  },
  'C8046': {
    'etiquette': 'J-M. Larrue',
    'departement': 'DLLF'
  },
  'C8044': {
    'etiquette': 'E. Nardout-Lafarge',
    'extra': '+ Nouvelle collègue littérature québécoise',
    'departement': 'DLLF'
  },
  'C8042': {
    'etiquette': 'S. Ménard',
    'departement': 'DLLF'
  },
  'C8040': {
    'etiquette': 'A. Oberhuber',
    'departement': 'DLLF'
  },
  'C8038': {
    'etiquette': 'S. Bernier',
    'departement': 'DLLF'
  },
  'C8036': {
    'etiquette': 'Revue études françaises / Figura',
    'extra': '<img src="img/etudes-francaises.png" /> <img src="img/figura.png" /> <br>Une partie du local est consacrée au centre FIGURA.',
    'departement': 'DLLF'
  },
  'C8034': {
    'etiquette': 'J-S. Desrochers',
    'departement': 'DLLF'
  },
  'C8032': {
    'etiquette': 'B. Wesley',
    'departement': 'DLLF'
  },
  'C8030': {
    'etiquette': 'B. Melançon',
    'departement': 'DLLF'
  },
  'C8028': {
    'etiquette': 'J. Bovet',
    'departement': 'DLLF'
  },
  'C8026': {
    'etiquette': 'M. Vitali Rosati',
    'departement': 'DLLF'
  },
  'C8024': {
    'etiquette': 'J. Semujanga',
    'departement': 'DLLF'
  },
  'C8022': {
    'etiquette': 'U. Dionne',
    'departement': 'DLLF'
  },
  'C8020': {
    'etiquette': 'J. Sribnai',
    'departement': 'DLLF'
  },
  'C8018': {
    'etiquette': 'R. Lauro',
    'departement': 'DLLF'
  },
  'C8012': {
    'etiquette': 'Professeurs retraités',
    'departement': 'DLLF'
  },
  'C8043': {
    'etiquette': 'Doctorants et postdoctorants',
    'departement': 'DLLF'
  },
  'C8041': {
    'etiquette': 'M. Vitali Rosati',
    'departement': 'DLLF'
  },
  'C8039': {
    'etiquette': 'Étudiants de maîtrise et revues étudiantes',
    'departement': 'DLLF'
  },
  'C8037': {
    'etiquette': 'C. Mavrikakis',
    'departement': 'DLLF'
  },
  'C8035': {
    'etiquette': 'K. Larose',
    'departement': 'DLLF'
  },
  'C8033': {
    'etiquette': 'M. Cambron',
    'extrae': '+Nouveau collègue en littérature française',
    'departement': 'DLLF'
  },
  'C8031': {
    'etiquette': 'Chaire de recherche du Canada sur les écritures numériques',
    'extra': '<img src="img/logo-crcen.png" />',
    'departement': 'DLLF'
  },
  'C8029': {
    'etiquette': 'Salle de réunion',
    'departement': 'DLLF'
  },
  'C8027': {
    'etiquette': 'CC DLLF',
    'departement': 'DLLF'
  },
  'C8025': {
    'etiquette': 'Étudiants au doctorat',
    'extra': '<img src="img/photos/doctorants2.jpg" />',
    'departement': 'DLLF'
  },
  'C8023': {
    'etiquette': 'S. Vachon',
    'departement': 'DLLF'
  },
  'C8019': {
    'etiquette': 'Le Soulier de Satin',
    'extra': 'Local de l’association étudiante <br> <img src="img/aellfum3.jpg" />',
    'departement': 'DLLF'
  },
  'C8009': {
    'etiquette': 'Cuisine',
    'departement': 'DLLF'
  },
  'C8007': {
    'etiquette': 'Fournitures',
    'departement': 'DLLF'
  },
};
