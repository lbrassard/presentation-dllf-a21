/* global SVG, Popper, document, Reveal*/
// SVG.js
const draw = SVG(document.getElementById('plan'))
// .panZoom();

// Naviguer au plan
let dernierClic = null;
const planBtn = document.getElementById('go-to-plan');

planBtn.addEventListener('click', togglePlanNav);

window.addEventListener('keypress', (ev) => {
  if (ev.key === '1') {
    togglePlanNav()
  }
});
Reveal.on( 'slidechanged', event => {

  if (event.indexh !== 1) {
    // plan
    dernierClic = null;
  }
} );

function togglePlanNav() {
  if (dernierClic) {
    Reveal.slide(dernierClic.h, dernierClic.v, dernierClic.f)
    dernierClic = null
  } else {
    dernierClic = Reveal.getIndices();
    console.log(dernierClic)
    Reveal.slide(1)
  }
}

// Popper.js
const popperEl = document.getElementById('popper');
const popperTitle = popperEl.querySelector('.popper__title');
const popperExtra = popperEl.querySelector('.popper__extra');
const popperMeta = popperEl.querySelector('.popper__meta');
let popperInstance;

for (const local of draw.find('.local')) {
  const numeroLocal = local.id()
  // draw.text(numeroLocal.toString())
  // .font({
  //   size: '1.25em'
  // })
  // .center(local.cx(), local.cy());

  local.on('mouseenter', () => {
    if (PLAN_DICTIONNAIRE[numeroLocal]) {
      popperTitle.innerHTML = PLAN_DICTIONNAIRE[numeroLocal].etiquette;
      if (PLAN_DICTIONNAIRE[numeroLocal].hasOwnProperty('extra')) {
        popperExtra.innerHTML = PLAN_DICTIONNAIRE[numeroLocal].extra ;
      } else {
        popperExtra.innerHTML = '';
      }
    } else {
      popperTitle.innerHTML = '';
    }

    popperMeta.innerHTML = numeroLocal;
    popperEl.style.visibility = 'visible'
    popperInstance = Popper.createPopper(local.node, popperEl, {
      placement: 'bottom',
      modifiers: [
        {
          name: 'offset',
          options: {
            offset: [0, 6],
          }
        },
        {
          name: 'fallbackPlacements',
          options: {
            fallbackPlacements: ['left', 'right', 'top']
          }
        },
        {
          name: 'preventOverflow',
          options: {
            boundary: document.querySelector('#plan')
          }
        }
      ]
    });
  });

  local.on('mouseleave', () => {
    popperEl.style.visibility = 'hidden'
  })
}
