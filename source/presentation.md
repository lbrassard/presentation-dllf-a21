---
title: "Présentation du DLLF"
date: 2021-10-22
author: Département des littératures de langue française
institute: Université de Montréal

# Reveal.js config
controls: false
controlsTutorial: false
hash: true
revealjs-url: "."
theme: dllf
center: true
backgroundTransition: fade
width: 1280
height: 700
margin: .15
---

<!-- ![Plan de l’étage](img/plan-8e-etage.svg) -->

---

## Le secrétariat {data-background-image="img/photos/couloir-secretariat.jpg" data-background-color="var(--r-background-color)" data-background-opacity=.25}

---

## {data-background-image="img/photos/couloir-secretariat.jpg" data-transition="fade"}

---

## {data-transition="fade" data-background-image="img/photos/secretariat1.jpg" data-background-size="contain"}

---

## {data-transition="fade" data-background-image="img/photos/secretariat2-carre.jpg" data-background-size=contain}

---

## {data-background-image="img/photos/livres-ensemble.jpg" data-background-size="contain"}

<!-- ![Vitrine des publications des professeurs](img/photos/livres-ensemble.jpg) -->

---

## {data-background-image="img/photos/livres4.jpg" data-background-transition="fade" data-background-size="contain" data-background-color="#000"}

## {data-background-image="img/photos/livres1.jpg" data-background-transition="fade" data-background-size="contain" data-background-color="#000"}

## {data-background-image="img/photos/livres3.jpg" data-background-transition="fade" data-background-size="contain" data-background-color="#000"}

## {data-background-image="img/photos/livres2.jpg" data-background-transition="fade" data-background-size="contain" data-background-color="#000"}

## {data-background-image="img/photos/livres6.jpg" data-background-transition="fade" data-background-size="contain" data-background-color="#000"}

---

## Le CRILCQ {data-background-image="img/collection-de-zines.jpg" data-background-opacity="0.2" data-transition="zoom"}

---

## {data-background-image="img/collection-de-zines.jpg" data-background-size="cover" data-transition=fade}

---

## {data-background-image="img/periodiques-et-poste-de-travail.jpg" data-background-size="cover" data-transition=fade}


## {data-background-image="img/theatrotheque.jpg" data-background-size="cover" data-transition=fade}

---

## {data-background-image="img/salle-de-documentation.jpg" data-background-size="contain" data-background-transition=fade}

---

## {data-background-image="img/evenement-scientifique1.jpg" data-background-size="contain" data-background-transition=fade}

<!-- ![Un événement scientifique](img/evenement-scientifique1.jpg) -->

---


## {data-background-image="img/evenement-scientifique2.jpg" data-background-size="contain" data-transition=fade}

<!-- ![Un événement scientifique](img/evenement-scientifique2.jpg) -->

---

## Le couloir principal {data-background-image="img/photos/couloir.jpg" data-background-color="#000" data-background-size="contain" data-background-opacity=".5" data-transition="zoom"}

## {data-background-image="img/photos/couloir.jpg" data-background-color="#000" data-background-size="contain" data-transition="none" data-background-transition="fade"}

---

## Le Soulier de Satin {data-background-color="#fff" data-background-image="img/aellfum3.jpg" data-background-opacity=.25 data-transition="zoom"}

---

## {data-background-image="img/aellfum3.jpg"}

<!-- ![Le local de l’AELLFUM](img/aellfum3.jpg) -->

---

<strong class="highlight">Le Soulier de Satin</strong> est le local destiné aux étudiant·e·s membres de l'Association des étudiant·e·s en littératures de langue française de l’Université de Montréal (AELLFUM) et à quiconque souhaitant faire connaissance avec eux·elles. Il se situe au local C-8019 au huitième étage du pavillon Lionel-Groulx de l'Université de Montréal.

---

:::::::::::::: {.columns}
::: {.column width="45%"}
![Le local de l’AELLFUM](img/aellfum1.jpg)
:::
::: {.column width="5%"}
:::
::: {.column width="50%"}
On y trouve des bibliothèques remplies de livres que les étudiant·e·s peuvent emprunter. <strong class="highlight">Il s'agit de l'endroit idéal pour se détendre, rencontrer ses collègues, étudier ou lire pendant les pauses et l'heure du dîner.</strong> Plusieurs activités sociales ou culturelles organisées par l'association étudiante, en plus des assemblées générales de l'AELLFUM, se déroulent d'ailleurs au soulier de satin.
:::
::::::::::::::

---

:::::::::::::: {.columns}
::: {.column width="45%"}
![Le local de l’AELLFUM](img/aellfum2.jpg)
:::
::: {.column width="5%"}
:::
::: {.column width="50%"}
Pendant la session, il est généralement ouvert du lundi au jeudi de 9h à 16h. Du thé et du café sont offerts gratuitement tout au long de la session. Le soulier de satin comporte <strong class="highlight">un réfrigérateur, ainsi qu'une imprimante</strong> accessible aux étudiant·e·s.
:::
::::::::::::::

---

## Le local des doctorants {data-background-image="img/photos/doctorants1.jpg" data-background-opacity="0.2" data-transition="zoom"}

---

## {data-background-image="img/photos/doctorants1.jpg" data-transition="fade"}

---

## {data-background-image="img/photos/doctorants2.jpg" data-background-size="contain" data-background-transition=fade}

---

## La Chaire de recherche du Canada sur les écritures numériques {data-background-image="img/crcen-site-web.png" data-background-opacity=".2" data-background-color="#121149" data-transition="zoom"}

---

## {data-background-image="img/crcen2.jpg" data-background-transition="fade" data-background-size="contain"}

<!-- ![Local de la CRCÉN](img/crcen2.jpg) -->

---

## {data-background-image="img/crcen1.jpg" data-background-transition="fade" data-background-size="contain"}

<!-- ![Local de la CRCÉN](img/crcen1.jpg) -->

---

## Matériel de la CRCÉN {data-background-color="#121149"}

Dans ces locaux, on compte le matériel suivant :

::: incremental

- une caméra *Meeting owl pro*
- 2 micros
- 1 imprimante
- 3 écrans
- 3 ordinateurs (1 Linux et 2 Mac).

:::

---

## Merci!
