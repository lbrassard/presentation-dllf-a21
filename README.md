# Présentation DLLF

Une présentation utilisant [Reveal.js](https://revealjs.com).

Marie-Pascale Huglo - directrice du DLLF à l’automne 2021.

## Dev

Requiert [Pandoc](http://pandoc.org).

```
source/
  -- presentation.md    # diapositives
  -- template.html      # modèle html (pandoc -> revealjs) 
```

```bash
./make-html.sh
```

Résultat: `index.html`.

[Modèle pandoc revealjs](https://github.com/jgm/pandoc-templates/blob/master/default.revealjs)

## Licence

WTFPL