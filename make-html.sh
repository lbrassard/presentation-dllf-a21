#!/bin/bash
pandoc --standalone \
  --template source/template.html \
  -t revealjs \
  source/plan-8e-etage.svg  \
  source/presentation.md  \
  -o index.html
